using System;

namespace examen_final_master
{
    class ExamenFinal
    {
        private string[] Nombres;
        private int[,] Sueldos;
        private int[] SueldosTOTALES;

        public void Cargar() 
        {
            Nombres=new String[5];
            Sueldos=new int[5,3];
            for(int f = 0; f < Nombres.Length; f++)
            {
                Console.Write("Ingrese el nombre del empleado:");
                Nombres[f]=Console.ReadLine();
                for(int c = 0; c < Sueldos.GetLength(1); c++) 
                {
                    Console.Write("Ingrese sueldo:");
                    string linea;
                    linea = Console.ReadLine();
                    Sueldos[f,c]=int.Parse(linea);
                }
            }
        }

        public void CalcularSumaSueldos()
        {
            SueldosTOTALES = new int[5];
            for (int f = 0; f < Sueldos.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < Sueldos.GetLength(1); c++)
                {
                    suma = suma + Sueldos[f,c];
                }
                SueldosTOTALES[f] = suma;
            }
        }

        public void ImprimirTotalPagado() 
        {
    	    Console.WriteLine("Total de sueldos pagados por empleado.");
            for(int f = 0; f < SueldosTOTALES.Length; f++) 
            {
                Console.WriteLine(Nombres[f]+" tiene un sueldo de: "+SueldosTOTALES[f]);
            }
        }

        public void EmpleadoMayorSueldo() 
        {
            int Mayor=SueldosTOTALES[0];
            string Nom=Nombres[0];
            for(int f = 0; f < SueldosTOTALES.Length; f++) 
            {
                if (SueldosTOTALES[f] > Mayor) 
                {
                    Mayor=SueldosTOTALES[f];
                    Nom=Nombres[f];
                }
            }
            Console.WriteLine("El empleado con mayor sueldo es "+ Nom + " que tiene un sueldo de "+Mayor);
        }

        static void Main(string[] args)
        {
            ExamenFinal Re = new ExamenFinal();
            Re.Cargar();
            Re.CalcularSumaSueldos();
            Re.ImprimirTotalPagado();
            Re.EmpleadoMayorSueldo();
            Console.ReadKey();
            
        }

    }
}
